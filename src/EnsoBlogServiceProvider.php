<?php

namespace Yadda\Enso\Blog;

use Illuminate\Support\ServiceProvider;
use Yadda\Enso\Blog\Contracts\Post as PostContract;
use Yadda\Enso\Blog\Contracts\PostCategory as PostCategoryContract;
use Yadda\Enso\Blog\Contracts\PostResource as PostResourceContract;
use Yadda\Enso\Blog\EnsoBlog;
use Yadda\Enso\Blog\Http\Resources\PostResource;
use Yadda\Enso\Blog\Models\Post as PostConcrete;
use Yadda\Enso\Blog\Models\PostCategory as PostCategoryConcrete;

class EnsoBlogServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom([
            __DIR__ . '/Database/Migrations/',
        ]);

        $this->publishes([
            __DIR__ . '/installable/resources/views' => resource_path('views'),
            __DIR__ . '/installable/config' => base_path('config'),
            __DIR__ . '/installable/resources/js' => resource_path('js/enso/blog'),
        ], 'enso-blog');

        $this->loadViewsFrom(__DIR__ . '/Resources/Views/', 'enso-blog');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('ensoblog', function () {
            return new EnsoBlog;
        });

        $this->app->bind(PostContract::class, PostConcrete::class);
        $this->app->bind(PostResourceContract::class, PostResource::class);
        $this->app->bind(PostCategoryContract::class, PostCategoryConcrete::class);
    }
}
