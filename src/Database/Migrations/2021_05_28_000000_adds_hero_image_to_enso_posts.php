<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddsHeroImageToEnsoPosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enso_posts', function (Blueprint $table) {
            $table->unsignedBigInteger('hero_id')->after('user_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enso_posts', function (Blueprint $table) {
            $table->dropColumn(['hero_id']);
        });
    }
}
