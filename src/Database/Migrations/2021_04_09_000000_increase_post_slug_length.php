<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class IncreasePostSlugLength extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enso_posts', function (Blueprint $table) {
            $table->renameColumn('slug', 'slug_char');
        });

        Schema::table('enso_posts', function (Blueprint $table) {
            $table->string('slug', 255)->nullable()->after('slug_char');
        });

        DB::table('enso_posts')
            ->update(['slug' => DB::raw('`slug_char`')]);

        Schema::table('enso_posts', function (Blueprint $table) {
            $table->dropColumn('slug_char');
        });

        Schema::table('enso_posts', function (Blueprint $table) {
            $table->string('slug', 255)->change();
        });
    }

    public function down()
    {
        // Reversing this migration would require truncating the strings
        // but the slug column has a unique index applied and we can't be sure
        // that the truncated values would be unique. Therefore I've chosen
        // not to roll this back.
    }
}
