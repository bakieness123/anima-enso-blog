<?php

namespace Yadda\Enso\Blog\Database\Seeders;

use Illuminate\Database\Seeder;
use Yadda\Enso\Blog\Models\Post;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::factory()->count(20)->create();
    }
}
