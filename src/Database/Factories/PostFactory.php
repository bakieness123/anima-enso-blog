<?php

namespace Yadda\Enso\Blog\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Yadda\Enso\Blog\Models\Post;
use Yadda\Enso\Media\Models\ImageFile;

class PostFactory extends Factory
{
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->sentence();
        $image_id = ImageFile::inRandomOrder()->value('id');

        return [
            'title' => $title,
            'slug' => Str::slug($title),
            'hero_id' => $image_id,
            'thumbnail_id' => $image_id,
            'featured' => false,
            'publish_at' => now(),
            'published' => true,
            'content' => [],
        ];
    }
}
