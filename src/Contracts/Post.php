<?php

namespace Yadda\Enso\Blog\Contracts;

use Illuminate\Database\Eloquent\Collection;

interface Post
{
    /**
     * Convert the model instance to an array.
     *
     * @return array
     */
    public function toArray();

    /**
     * Get related posts. If the number of related posts is less
     * than than the value of max_related_posts then recent
     * posts will be used to make up the amount
     */
    public function getRelated(): Collection;
}
