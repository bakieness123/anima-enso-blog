<?php

namespace Yadda\Enso\Blog\Contracts;

interface PostResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request);
}
