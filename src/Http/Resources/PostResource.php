<?php

namespace Yadda\Enso\Blog\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Yadda\Enso\Blog\Contracts\PostResource as ContractsPostResource;

class PostResource extends JsonResource implements ContractsPostResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
