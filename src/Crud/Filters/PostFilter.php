<?php

namespace Yadda\Enso\Blog\Crud\Filters;

use Yadda\Enso\Crud\Filters\BaseFilters\TextFilter;

class PostFilter extends TextFilter
{
    /**
     * Columns to search in
     *
     * @var array
     */
    protected $columns = [
        'title',
    ];

    /**
     * Label to apply to the filter
     *
     * @var string
     */
    protected $label = 'Post Search';

    /**
     * Props to apply to the filter
     *
     * @var array
     */
    protected $props = [
        'placeholder' => 'Search...',
        'help-text' => 'Search by title',
    ];
}
