<?php

namespace Yadda\Enso\Blog\Crud;

use Illuminate\Database\Query\Builder;
use Yadda\Enso\Blog\Contracts\Post as PostContract;
use Yadda\Enso\Blog\Contracts\PostCategory;
use Yadda\Enso\Crud\Config;
use Yadda\Enso\Crud\Contracts\Config\IsPublishable as ConfigIsPublishable;
use Yadda\Enso\Crud\Forms\FieldInterface;
use Yadda\Enso\Crud\Forms\Fields\BelongsToField;
use Yadda\Enso\Crud\Forms\Fields\BelongsToManyField;
use Yadda\Enso\Crud\Forms\Fields\CheckboxField;
use Yadda\Enso\Crud\Forms\Fields\DateTimeField;
use Yadda\Enso\Crud\Forms\Fields\FileUploadFieldResumable;
use Yadda\Enso\Crud\Forms\Fields\FlexibleContentField;
use Yadda\Enso\Crud\Forms\Fields\SelectField;
use Yadda\Enso\Crud\Forms\Fields\SlugField;
use Yadda\Enso\Crud\Forms\Fields\TextField;
use Yadda\Enso\Crud\Forms\Form;
use Yadda\Enso\Crud\Forms\Section;
use Yadda\Enso\Crud\Tables\Publish;
use Yadda\Enso\Crud\Tables\Text;
use Yadda\Enso\Crud\Traits\Config\HasTemplates;
use Yadda\Enso\Crud\Traits\Config\IsPublishable;
use Yadda\Enso\Crud\Traits\HasDefaultRowSpecs;
use Yadda\Enso\Meta\Crud\MetaSection;
use Yadda\Enso\Users\Contracts\User;
use Yadda\Enso\Utilities\Helpers;

class Post extends Config implements ConfigIsPublishable
{
    use HasDefaultRowSpecs,
        HasTemplates,
        IsPublishable;

    const DEFAULT_PAGINATION = 25;

    /**
     * Configure the CRUD
     *
     * @return void
     */
    public function configure()
    {
        $this
            ->model(Helpers::getConcreteClass(PostContract::class))
            ->route('admin.posts')
            ->views('posts')
            ->name('Post')
            ->searchColumns(['title'])
            ->order('id', 'DESC')
            ->paginate(static::DEFAULT_PAGINATION)
            ->columns([
                Text::make('title'),
                Text::make('publish_at')
                    ->setFormatter(function ($value) {
                        if ($value) {
                            return $value->format('jS M Y G:i');
                        }

                        return '—';
                    }),
                Publish::make('is_published', $this)
                    ->setLabel('Published')
                    ->addThClass('is-narrow has-text-centered'),
            ])
            ->rules([
                'main.title' => 'required|string|max:255',
                'main.template' => 'required',
            ])
            ->filters([
                'search' => \Yadda\Enso\Blog\Crud\Filters\PostFilter::make(),
                'user' => \Yadda\Enso\Crud\Filters\UserFilter::make()
                    ->label('Author')
                    ->relationshipName('user'),
            ]);
    }

    /**
     * Default form configuration.
     */
    public function create(Form $form)
    {
        $post_model = config('enso.crud.post.model');
        $post_category_class = Helpers::getConcreteClass(PostCategory::class);

        $form->addSections([
            Section::make('main')
                ->addFields([
                    TextField::make('title')
                        ->addFieldsetClass('is-half'),
                    SlugField::make('slug')
                        ->addFieldsetClass('is-half')
                        ->setRoute($this->slugRoute())
                        ->setSource('title'),
                    DateTimeField::make('publish_at'),
                    SelectField::make('template')
                        ->setLabel('Page Template')
                        ->setOptions($this->hasTemplatesList($post_model)),
                    CheckboxField::make('featured')
                        ->setOptions([
                            'featured' => 'This post should appear before other posts',
                        ]),
                    $this->getHeroImageField(),
                    $this->getThumbnailImageField(),
                    BelongsToManyField::make('categories')
                        ->addFieldsetClass('is-half-desktop')
                        ->useAjax(
                            route('admin.categories.index'),
                            $post_category_class
                        ),
                    BelongsToManyField::make('related')
                        ->setLabel('Related Posts')
                        ->addFieldsetClass('is-half-desktop')
                        ->setSetting('max', config('enso.blog.max_related_posts'))
                        ->useAjax(
                            route('admin.posts.index'),
                            $post_category_class
                        ),
                ]),
            Section::make('content')
                ->addFields([
                    FlexibleContentField::make('content')
                        ->addRowSpecs(
                            $this->defaultRowSpecs(),
                        ),
                ]),
            MetaSection::make(),
        ]);

        return $form;
    }

    /**
     * Create an edit form.
     *
     * @return \Yadda\Enso\Crud\Forms\Form
     */
    public function edit(Form $form)
    {
        $form = $this->create($form);

        $form->getSection('main')
            ->addFieldBefore(
                'template',
                BelongsToField::make('user')
                    ->setLabel('Author')
                    ->useAjax(
                        route('admin.users.index'),
                        Helpers::getConcreteClass(User::class)
                    )
                    ->addFieldsetClass('is-half'),
            );

        $form->getSection('main')
            ->getField('template')
            ->addFieldsetClass('is-half');

        return $form;
    }

    /**
     * Apply a search term to a database query
     */
    public function postSearch(Builder $query, string $term): void
    {
        $query->where('title', 'LIKE', '%' . $term . '%');
    }

    /**
     * Hero Image field
     *
     * @return FieldInterface
     */
    protected function getHeroImageField(): FieldInterface
    {
        return FileUploadFieldResumable::make('hero_id')
            ->setLabel('Hero Image')
            ->setRelationshipName('hero')
            ->setUploadPath('posts/heroes')
            ->addFieldsetClass('is-half');
    }

    /**
     * Hero Image field
     *
     * @return FieldInterface
     */
    protected function getThumbnailImageField(): FieldInterface
    {
        return FileUploadFieldResumable::make('thumbnail_id')
            ->setLabel('Thumbnail Image')
            ->setRelationshipName('thumbnail')
            ->setUploadPath('posts/thumbnails')
            ->addFieldsetClass('is-half');
    }
}
