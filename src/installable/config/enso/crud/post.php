<?php

return [

    /**
     * Class of the Crud Config for Posts.
     */
    'config' => \Yadda\Enso\Blog\Crud\Post::class,

    /**
     * Class of the Crud Controller for Posts.
     */
    'controller' => \Yadda\Enso\Blog\Controllers\Admin\PostController::class,

    /**
     * Properties for the Ensō menu item for Posts.
     */
    'menuitem' => [
        'icon' => 'fa fa-files-o',
        'label' => 'Posts',
        'route' => ['admin.posts.index'],
    ],

    /**
     * Class of the Crud Model for Posts.
     */
    'model' => \Yadda\Enso\Blog\Models\Post::class,

];
