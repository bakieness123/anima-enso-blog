<?php

namespace Yadda\Enso\Blog\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Facade for Enso Blog
 */
class EnsoBlog extends Facade
{
    /**
     * Get the facade accessor
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'ensoblog';
    }
}
