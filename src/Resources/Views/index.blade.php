@extends(Config::get('enso.blog.layout_view'))

@section('content')
  <blog-index
    category-url="{{ $category_url }}"
  ></blog-index>
@endsection
