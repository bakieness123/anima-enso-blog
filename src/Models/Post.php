<?php

namespace Yadda\Enso\Blog\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Yadda\Enso\Blog\Contracts\Post as PostModelContract;
use Yadda\Enso\Blog\Database\Factories\PostFactory;
use Yadda\Enso\Categories\Traits\BelongsToManyCategories;
use Yadda\Enso\Crud\Contracts\Model\IsPublishable as ModelIsPublishable;
use Yadda\Enso\Crud\Traits\HasFlexibleFields;
use Yadda\Enso\Crud\Traits\IsCrudModel;
use Yadda\Enso\Crud\Traits\Model\HasTemplates;
use Yadda\Enso\Crud\Traits\Model\IsPublishable;
use Yadda\Enso\Media\Contracts\ImageFile;
use Yadda\Enso\Media\Contracts\MediaFile;
use Yadda\Enso\Media\Traits\HasFilesTrait;
use Yadda\Enso\Meta\Traits\HasMeta;
use Yadda\Enso\Users\Contracts\User;
use Yadda\Enso\Utilities\Helpers;

class Post extends Model implements ModelIsPublishable, PostModelContract
{
    use BelongsToManyCategories,
        HasFilesTrait,
        HasFlexibleFields,
        HasMeta,
        HasTemplates,
        IsCrudModel,
        IsPublishable,
        HasFactory;

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'is_published',
        'url',
    ];

    /**
     * The model's attributes
     *
     * @var array
     */
    protected $attributes = [
        'content' => '[]',
        'template' => 'default',
    ];

    /**
     * Attributes that should be cast to native types
     *
     * @var array
     */
    protected $casts = [
        'content' => 'array',
        'featured' => 'boolean',
        'hero_id' => 'integer',
        'published' => 'boolean',
        'publish_at' => 'datetime',
        'thumbnail_id' => 'integer',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content',
        'featured',
        'hero_id',
        'published',
        'publish_at',
        'slug',
        'thumbnail_id',
        'title',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
    ];

    /**
     * Name of the database table
     *
     * @var string
     */
    protected $table = 'enso_posts';

    /**
     * The "booting" method of the model.
     */
    public static function boot(): void
    {
        parent::boot();

        if (Config::get('enso.blog.force_post_authors', true)) {
            static::saving(function ($model) {
                if (is_null($model->user_id)) {
                    $model->user_id = Auth::id();
                }
            });
        }
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return PostFactory::new();
    }

    /**
     * Categories that this post is in
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(
            Helpers::getConcreteClass(PostCategory::class),
            'enso_category_post',
            'post_id',
            'category_id'
        );
    }

    /**
     * Name of crud to use for categories
     *
     * @return string
     */
    public static function getCategoryCrud(): string
    {
        return 'category';
    }

    /**
     * Hero image, falling back to thumbnail image if not provided.
     *
     * @return MediaFile|null
     */
    public function getHeroImage(): ?MediaFile
    {
        return $this->hero
            ? $this->hero
            : $this->thumbnail;
    }

    /**
     * Gets the name of the Publish At DateTime column on this publishable
     *
     * @return string|null
     */
    public function getPublishAtColumn(): ?string
    {
        return 'publish_at';
    }

    /**
     * Name of the permission that allows users to view a page based on
     * irrespective of publishing state.
     *
     * @return string|null
     */
    public function getPublishViewOverridePermission()
    {
        return 'view-unpublished-posts';
    }

    /**
     * Get related posts. If the number of related posts is less
     * than than the value of max_related_posts then recent
     * posts will be used to make up the amount
     */
    public function getRelated(): Collection
    {
        $related = $this->related()->accessibleToUser()->get();
        $total = config('enso.blog.max_related_posts');
        $exclude_ids = $related->pluck('id');
        $exclude_ids->push($this->id);

        if ($related->count() > $total) {
            return $related->slice(0, $total);
        }

        if ($related->count() === $total) {
            return $related;
        }

        return $related->concat(
            self::query()
                ->accessibleToUser()
                ->whereNotIn('id', $exclude_ids)
                ->orderBy('publish_at', 'DESC')
                ->take($total - $related->count())
                ->get()
        );
    }

    /**
     * Get the route key for the model.
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    /**
     * Thumbnail image, falling back to hero image if not provided.
     *
     * @return MediaFile|null
     */
    public function getThumbnailImage(): ?MediaFile
    {
        return $this->thumbnail
            ? $this->thumbnail
            : $this->hero;
    }

    /**
     * Public URL for this post. In instances of new (unsaved) posts, returns
     * an empty string
     */
    public function getUrlAttribute(): string
    {
        if (is_null($this->slug)) {
            return '';
        }

        return route(config('enso.blog.public_route', 'posts') . '.show', $this->slug);
    }

    /**
     * Hero image for this Post
     *
     * @return BelongsTo
     */
    public function hero(): BelongsTo
    {
        return $this->belongsTo(
            Helpers::getConcreteClass(ImageFile::class),
            'hero_id'
        );
    }

    /**
     * Related posts
     */
    public function related(): BelongsToMany
    {
        return $this->belongsToMany(
            get_class($this),
            'enso_post_post',
            'post_id',
            'related_post_id'
        );
    }

    /**
     * A related thumbnail image
     */
    public function thumbnail(): BelongsTo
    {
        return $this->belongsTo(
            Helpers::getConcreteClass(ImageFile::class),
            'thumbnail_id'
        );
    }

    /**
     * Names of presets to make available for thumbnails
     *
     * @return array
     */
    public function thumbnailSizes(): array
    {
        return Config::get('enso.blog.thumbnail_sizes', [
            'default' => '768_x',
        ]);
    }

    /**
     * Convert to an array
     */
    public function toArray(): array
    {
        $array = parent::toArray();

        if (!$this->thumbnail) {
            $array['thumbnail'] = null;
        } else {
            $array['thumbnail'] = [
                'alt_text' => $this->thumbnail->alt_text,
                'urls' => []
            ];

            foreach ($this->thumbnailSizes() as $name => $preset) {
                $array['thumbnail']['urls'][$name] = $this->thumbnail->getResizeUrl($preset, true);
            }
        }

        if ($this->relationLoaded('user')) {
            $array['user'] = $this->user ? $this->user->toArray() : null;
        }

        return $array;
    }

    /**
     * The user who owns this post
     *
     * By default this is set to whoever created the post but can be manually
     * updated in the admin area later
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(
            Helpers::getConcreteClass(User::class),
            'user_id'
        );
    }
}
