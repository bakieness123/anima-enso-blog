<?php

namespace Yadda\Enso\Blog\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\App;
use Yadda\Enso\Blog\Contracts\Post;
use Yadda\Enso\Blog\Contracts\PostCategory as ContractsPostCategory;
use Yadda\Enso\Categories\Models\Category;

class PostCategory extends Category implements ContractsPostCategory
{
    /**
     * Name of the database table
     *
     * @var string
     */
    protected $table = 'enso_categories';

    /**
     * Posts in this category
     */
    public function posts(): BelongsToMany
    {
        return $this->belongsToMany(
            App::make(Post::class),
            'enso_category_post',
            'category_id',
            'post_id'
        );
    }
}
