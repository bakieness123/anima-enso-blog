<?php

namespace Yadda\Enso\Blog\Controllers\Admin;

use Yadda\Enso\Crud\Controller;
use Yadda\Enso\Crud\Traits\Controller\HasTemplates;
use Yadda\Enso\Crud\Traits\Controller\IsPublishable;

class PostController extends Controller
{
    use HasTemplates,
        IsPublishable;

    /**
     * Name of this crud type
     *
     * This will be used to find config/model/controller
     *
     * @var string
     */
    protected $crud_name = 'post';
}
